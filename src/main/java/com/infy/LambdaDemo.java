package com.infy;

interface Demo
{
	void display();
}

public class LambdaDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Demo demo= ()-> System.out.println("In Interface Implementation");
		
		demo.display();
				
	}

}
